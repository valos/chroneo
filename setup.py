# -*- coding: utf-8 -*-

# Chroneo -- A Stopwatch and Timer application
#
# Copyright (C) 2010-2012 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/chroneo/
#
# This file is part of Chroneo.
#
# Chroneo is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Chroneo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup

def main():
    setup(name         = 'chroneo',
          version      = '1.0.2',
          description  = 'a Stopwatch and Timer application',
          author       = 'Valery Febvre',
          author_email = 'vfebvre@easter-eggs.com',
          url          = 'http://code.google.com/p/chroneo/',
          classifiers  = [
            'Development Status :: 5 - Production/Stable',
            'Environment :: X11 Applications',
            'Intended Audience :: End Users/Phone UI',
            'License :: GNU General Public License (GPL)',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: Desktop Environment',
            ],
          packages     = ['chroneo'],
          scripts      = ['chroneo/chroneo'],
          data_files   = [
            ('share/applications', ['data/chroneo.desktop']),
            ('share/pixmaps', ['data/chroneo.png']),
            ('share/chroneo', ['README', 'data/chroneo.edj']),
            ('share/chroneo/sounds', ['data/sounds/alarm.wav']),
            ],
          )

if __name__ == '__main__':
    main()
