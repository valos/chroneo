DESCRIPTION = "A Stopwatch and Timer application"
HOMEPAGE = "http://code.google.com/p/chroneo/"
LICENSE = "GPLv3"
AUTHOR = "Valéry Febvre <vfebvre@easter-eggs.com>"
SECTION = "x11/applications"
PRIORITY = "optional"

SRCREV = "9"
PV = "1.0.2+svnr${SRCPV}"

PACKAGE_ARCH = "all"

SRC_URI = "svn://chroneo.googlecode.com/svn;module=trunk;proto=http"
S = "${WORKDIR}/trunk"

inherit distutils

FILES_${PN} += "${datadir}/chroneo ${datadir}/applications/chroneo.desktop ${datadir}/pixmaps/chroneo.png"

RDEPENDS += "python-audio python-pyalsaaudio python-elementary python-sqlite3"

do_compile_prepend() {
	${STAGING_BINDIR_NATIVE}/edje_cc -id ${S}/data ${S}/data/chroneo.edc
}
