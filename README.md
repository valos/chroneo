      ____ _
     / ___| |__  _ __ ___  _ __   ___  ___  
    | |   | '_ \| '__/ _ \| '_ \ / _ \/ _ \
    | |___| | | | | | (_) | | | |  __/ (_) |
     \____|_| |_|_|  \___/|_| |_|\___|\___/

Description
===========
Chroneo is a Stopwatch and Timer application for mobile devices
(particularly the Openmoko phone Neo Freerunner and the SHR/FSO
distribution).

It's written in Python / Elementary.

https://gitlab.com/valos/chroneo


Features
========
- Stopwatch
- Timer


Authors
=======
- Valéry Febvre <vfebvre@easter-eggs.com>


Contributors
============
- Benjamin Deering <ben_deering@swissmail.org>


Requirements
============
- python-audio
- python-elementary
- python-pyalsaaudio
- python-sqlite3


Report Bugs
===========
If there is something wrong, please use to the Google code issues:
https://gitlab.com/valos/chroneo/issues
Report a bug or look there for possible workarounds.


Credits
=======
- Suzanna Smith and Calum Benson for High Contrast icons (available in
  Gnome accessibility themes)


Version History
===============

2012-04-12 - Version 1.0.2
--------------------------
- Release due to Elementary API changes

2010-11-17 - Version 1.0.1
--------------------------
- Major bugfixes release due to a Elementary API change in Toolbar widget

2010-04-03 - Version 1.0.0
--------------------------
- First release.
